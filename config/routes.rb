Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # root 'users#new'

  resources :users do
  	collection do
  		get 'login' => 'users#try_login'
  		post 'login' => 'users#login'
  		get 'log_out'
  	end
  end

  resources :articles

end
