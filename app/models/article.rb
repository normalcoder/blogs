class Article < ApplicationRecord
	validates :title, presence: true

	belongs_to :user
	has_many :article_contents


	def get_content
	 	content = ArticleContent.find_by_id(self.content_id).content
	 	content
	 end 
end
