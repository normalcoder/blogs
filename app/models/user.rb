class User < ApplicationRecord
	validates :login, presence: { message: "用户名不能为空" }
	# format: { with: /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/, message: "用户名必须包含大小写字母和数字" }
	validates :email, presence: { message: "邮箱不能为空" }
	validates :password, presence: { message: "密码不能为空" }

	has_many :articles
end
