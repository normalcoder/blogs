class ArticlesController < ApplicationController

	# before_action :insure_logged_in

	def index
		@articles = current_user.articles
	end

	def new
		@article = Article.new
	end

	def create
		redirect_to new_article_path and return if params[:content].blank?
		@article = Article.new(params.require(:article).permit(:title, :user_id))
		if @article.save
			ac = ArticleContent.new
			ac.content = params[:content]
			ac.article_id = @article.id
			ac.save
			@article.update_attributes(content_id: ac.id)
			redirect_to articles_path
		else
			render 'new'
		end
	end

	def edit
		@article = Article.find_by_id(params[:id])
	end

	def update
		@article = Article.find_by_id(params[:id])
		content_id = @article.content_id
		@ac = ArticleContent.find_by_id(@article.content_id)
		if @ac.content != params[:content]
			ac = ArticleContent.new
			ac.content = params[:content]
			ac.article_id = @article.id
			ac.save
			@article.update_attributes(content_id: ac.id)
		end
		@article.update_attributes(title: params[:article][:title])
		@article.update_attributes(content_id: params[:article][:content_id]) if params[:article][:content_id].to_i != content_id
		redirect_to articles_path
	end

	def show
		@article = Article.find_by_id(params[:id])
	end

	def destroy
		##
	end


	private

	def insure_logged_in
		unless current_user
			redirect_to log_in_users_path
		end
	end
end
