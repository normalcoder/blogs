class UsersController < ApplicationController

	def index
		
	end

	def new
		@user = User.new
	end

	def create
		params[:user][:password] = Digest::SHA1.hexdigest(params[:user][:password]) if !params[:user][:password].blank?
		@user = User.new(user_params)
		if @user.save
			redirect_to login_users_path
		else
			render 'new'
		end
	end

	def show
		@user = User.find_by_id(params[:id])
	end

	def try_login
	end

	def login
		@user = User.find_by_login(params[:login]) || User.find_by_email(params[:login])
		if @user && @user.password == Digest::SHA1.hexdigest(params[:password])
			flash[:danger] = "请勿重复登录" and redirect_to users_path and return if logged_in?
			log_in @user
			redirect_to users_path
		else
		    flash[:danger] = "用户名或者密码错误，请确认后重试"
		    render 'try_login'
		end
	end

	def log_out
		logout if logged_in?
		redirect_to new_user_path
	end

	def destroy
		
	end

	private

	def user_params
		params.require(:user).permit(:email, :login, :password)
	end
end
